// export const changeName = (name) => {
//     return {
//         type: 'CHANGE_NAME', payload: name
//     }
// }

export const changeName = () => {
    return (dispatch) => {
        fetch('https://jsonplaceholder.typicode.com/users')
        .then(response => response.json())
        .then(json => {
           dispatch({  type: 'CHANGE_NAME', payload: json[0].name })
        })
    }
}