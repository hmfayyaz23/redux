import React, { Component } from 'react';
import { connect } from 'react-redux';
import { changeName } from './actiontypes/actions';

class App extends Component{
    constructor(props){
        super(props);
        this.state = {
        }
    }

    render(){
        const {name, handleChangeName} = this.props;
        return(
            <div>
                <h1>{name}</h1>
                <button onClick={() => handleChangeName()} >click me!</button>
            </div>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        name: state.name
    };
} 

const mapDispatchToProps = (dispatch) => {
    return{
        handleChangeName: () => dispatch(changeName())
    };
}

export default connect(mapStateToProps, mapDispatchToProps)(App);