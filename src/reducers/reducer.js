const iState = {
    name: 'fayyaz',
    code: [1, 2]
};

const reducer = (state=iState, action) => {
    switch(action.type){
        case 'CHANGE_NAME':
            return { ...state, name: action.payload };
        default:
            return state;
    }
};

export default reducer;